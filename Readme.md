# QC1284B Firmware
Firmware repository for QC12864B LCD display in C.

### Getting started
Download or clone qc12864b folder in your modules path directory.

### Prerequisites
No prerequisites

### Usage
User should start by checking and editing as needed the following header files.

* **qcTypes** : This header file has all the qc12864b configurations for enabling different features.
* **qcHardware.h** : All hardware related definitions, macro and header files needed for SPI control and communication to QC12864B LCD. 
This file must be edited by the user to add the necessary dependencies.

Other header files.

* **qcLCD.h** : Application header file for QC12864B usage. Include in your application header file.
* **qcBmp.h** : Static bitmap image generation. Includes an static array for user edition.
* **qcSignalPlot.h** : Tools for using the QC12864B display for plotting time variant signals. 

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
Licensed under the GPL License

## Authors
* **Federico Baigorria** - [fbaigorria](https://bitbucket.org/fbaigorria92)
