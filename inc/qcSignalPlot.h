/*!
 *  @file 	   qcSignalPlot.h
 *  @brief     QC12864B application header file. DO NOT EDIT.
 *  @details   Include this header for plotting variant time signals.
 *
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __QC12864B_INC_QCSIGNALPLOT_H__
#define __QC12864B_INC_QCSIGNALPLOT_H__

#if QC_USE_SIGNAL

///! QC LCD plot macros for dots
#define QC_DOT_MASK			0xC000
#define QC_PLOT_X_ORIG 		0x0800
#define	QC_PLOT_Y_ORIG 		0x0800
#define QC_X_DOT_OFFS		3

///! Define constants for scaling Y values to Y dots
#define	QC_PLOT_MAX_YDOT 	63
#define QC_PLOT_MIN_YDOT	6
#define	QC_PLOT_DELTAYDOT	(QC_PLOT_MAX_YDOT - QC_PLOT_MIN_YDOT)
#define QC_PLOT_DELTAYVAL	(QC_PLOT_MAX_YVAL - QC_PLOT_MIN_YVAL)
#define QC_PLOT_LINE_A		(float) (QC_PLOT_DELTAYDOT / QC_PLOT_DELTAYVAL)
#define QC_PLOT_LINE_B		(float) (QC_PLOT_MIN_YDOT - QC_PLOT_LINE_A * QC_PLOT_MIN_YVAL)

static qcPlotY qcDots[8];
static uint8_t qcXptr;
static uint8_t qcDotidx;

///! Private functions
static void qcSetPlotAxis(void);
static void qcSortDots(void);
static uint8_t qcRoundNewVal(void);
static void qcFormatNewDot(void);
static void qcPlotNewDot(void);

#endif /* QC_USE_SIGNAL */

#endif /* __QC12864B_INC_QCSIGNALPLOT_H__ */
