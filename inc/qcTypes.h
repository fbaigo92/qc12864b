/*!
 *  @file 	   qcTypes.h
 *  @brief     QC12864B types.
 *  @details   QC types definition for image array management and rows. Configurations defines
 *  		   must be configured here.
 *
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __INC_QCTYPES_H__
#define __INC_QCTYPES_H__

#define QC_FRTOS			1
#define	QC_MEM_DYN			1
#define QC_USE_BMP			1
#define QC_USE_SIGNAL		1
#define	QC_USE_TEST			1

///! User defined maximum and minimum values used by qcSignalPlot(). Set as float
#define	QC_PLOT_MAX_YVAL	100.0f
#define	QC_PLOT_MIN_YVAL	0.0f

///! Type definitions
///! Rows enum, it can also be used the constant definitions in qcLCD.h
typedef enum{
	qcRow1,
	qcRow2,
	qcRow3,
	qcRow4,
} qcRows;

typedef enum{
	qcRowBase_1 = 0x80,
	qcRowBase_2 = 0x90,
	qcRowBase_3 = 0x88,
	qcRowBase_4 = 0x98,
} qcRowBase;

typedef struct{
	uint8_t qcYaddr;
	uint8_t qcXoffs;
	uint16_t qcPixel;
} qcPlotY;

///! QC Array image struct (for dynamic management)
typedef struct{
	uint16_t qcPixel;
	uint8_t qcYaddr;
	uint8_t qcXaddr;
} qcLCDArray;

#endif /* __INC_QCTYPES_H__ */
