/*!
 *  @file 	   qcLCD.h
 *  @brief     QC12864B application header file. DO NOT EDIT.
 *  @details   Include this header in your application display control file.
 *
 *	@warning   QC12864B qcInit() function needs that all the required peripheral and pins to
 *			   be previously set and configured by user's application. Call this function after
 *			   hardware initialization only. Refer to qc12864b.h file.
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef _QC_LCD_H_
#define _QC_LCD_H_

#include "qcTypes.h"

///! Coordinates for text
#define	QC_ROW1_BASE	0x80	///! First row start coordinate
#define	QC_ROW2_BASE	0x90	///! Second row start coordinate
#define	QC_ROW3_BASE	0x88	///! Third row start coordinate
#define	QC_ROW4_BASE	0x98	///! Fourth row start coordinate

///! Coordinates for highlighting rows
#define	QC_ROW1			0		///! Highlight row 1
#define	QC_ROW2			1		///! Highlight row 1
#define	QC_ROW3			2		///! Highlight row 1
#define	QC_ROW4			3		///! Highlight row 1

///! Public functions
void qcInit(void);
void qcClear(void);
void qcSetText(char *, uint8_t);
void qcSetHighlight(qcRows);
void qcClearHighlight(qcRows);

///! Public functions (must be enabled)
#if QC_USE_TEST
void qcTest(void);
#endif

#if QC_MEM_DYN
void qcSetDynamicImage(qcLCDArray, uint8_t);
#endif /* QC_MEM_DYN */

#if QC_USE_BMP
void qcSetImage(void);
#endif /* QC_USE_BMP */

#if QC_USE_SIGNAL
void qcSignalPlot(void);
#endif /* QC_USE_SIGNAL */

#endif /* _QC_LCD_H_ */
