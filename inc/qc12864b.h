/*!
 *  @file 	   qc12864b.h
 *  @brief     QC12864B 128x64 LCD library.
 *  @details   User can set different parameters detailed below.
 *  		   If an OS is used set QC_FRTOS to 1 (only portable for FreeRTOS)
 *  		   If dynamic image generation is needed set QC_MEM_DYN to 1
 *
 *			   Header files:
 *			   qc12864b.h 	Functionality configuration for QC12864B LCD control
 *			   qcBmp.h	  	Static bitmap image generation. Includes an static array for user edition
 *			   qcHardware.h All hardware related definitions, macro and header files needed for
 *			   				SPI control and communication to QC12864B LCD.
 *			   qcLCD.h		Application header file for QC12864B usage.
 *	@warning   Needs to be edited by the user accordingly
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __QC12864_LCD_H__
#define __QC12864_LCD_H__

/** DO NOT EDIT
 */

///! Self header files
#include "qcHardware.h"
#include "qcTypes.h"

#if QC_USE_SIGNAL
#include "qcSignalPlot.h"
#endif /* QC_USE_SIGNAL */

///! Delay modules and constants
#define	QC_CLEAR_DLY 		2		///! ms. Refer to QC12864B for this (1.6ms delay for 560kHz)
#define QC_REFRESH_GPH		1000	///! ms

#if	QC_FRTOS
	///! FreeRTOS includes
	#include "FreeRTOS.h"
	#include "task.h"
#else
	/** Delay done flag. An external system must set this flag (true = 1) once the clear time
	 *  delay is accomplished.
	 */
	bool qcDelay;
#endif

///! QC12864B expected datagram length (3 bytes)
#define	QC_DATA_LENGHT 3

///! QC12864B LCD modes
#define QC_IRM			0xF8
#define QC_DTM			0xFA

typedef enum{
	qcIRM = 0xF8, 	///! RW 1 RS 0 - Instruction mode
	qcDTM = 0xFA,	///! RW 0 RS 1 - Data mode
} qcModes;

///! Commands
#define QC_SET_BASIC  	0x20	///! 4 bit, basic instruction - graphic off
#define	QC_SET_EXT	 	0x24	///! 4 bit, extended instruction - graphic off
#define	QC_GPH_ON	 	0x26	///! 4 bit, extended instruction - graphic on
#define	QC_DISP_ON  	0x0F    ///! Display ON, Blink ON, Cursor ON
#define	QC_CLEAR  		0x01	///! Display clear
#define	QC_ENTRY	 	0x06	///! Entry mode - shift right by 1
#define	QC_DISP_CTRL	0x0C	///! Display control - display ON, cursor off, blink off
#define	QC_SET_DDRAM	0x80	///! Set DDRAM Address - first position at 0x80

///! LCD display position macros
#define ROW_BASE 		0x80
#define ROW_OFFSET	 	0x08
#define MAX_HOR_ADDRESS 16
#define MAX_VER_ADDRESS	32

///! Private functions
static void qcWrite(qcModes, uint8_t);
static void qcTextClear(void);
static void qcSetPixel(uint8_t, uint8_t, uint16_t);
static void qcSetRow(qcRows, uint16_t );

#endif /* __QC12864_LCD_H__ */
