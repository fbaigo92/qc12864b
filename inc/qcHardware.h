/*!
 *  @file 	   qc12864b.h
 *  @brief     QC12864B 128x64 LCD library.
 *  @details   Hardware dependencies needed for SPI communication. This header file can be used
 *  		   in application defined hardware initialization for SPI.
 *
 *			   Includes dependencies for hardware definitions
 *  		   Set SPI hardware pinout and peripheral port
 *  		   If needed set the logic TRUE, FALSE macro values defined
 *  		   Set SPI transmit enable and disable functions macros
 *  		   Set SPI buffer load function macro
 *  @warning   Needs to be edited by the user accordingly. Add uint_Xt type definitions here
 *  @author    Federico Baigorria
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#ifndef __QC12864B_INC_QCHARDWARE_H_
#define __QC12864B_INC_QCHARDWARE_H_

/* Include here all the board and hardware header files needed for SPI peripheral
 * control and communication
 */
#include "board.h"

///! Hardware macro definitions
#define	QC_SSP				LPC_SSP0	///! SSP port to be used
#define	QC_CS_PIN			0,1			///! RS pin (according to QC12864B)
#define QC_RST_PIN			0,16		///! RST pin (according to QC12864B)
#define QC_EN_PIN 			0,15		///! SCK pin (according to QC12864B)
#define	QC_RW_PIN			0,18		///! MOSI pin (according to QC12864B)

///! Logic definitions
#define	QC_TRUE				TRUE
#define QC_FALSE			FALSE

///! Hardware macro functions
#define	QC_ENABLE_SPI(X)	Chip_GPIO_WritePortBit(LPC_GPIO, QC_CS_PIN, (X))
#define QC_SEND_SPI(X, Y)	Chip_SSP_WriteFrames_Blocking(QC_SSP, (X), (Y))
#define QC_PINRESET(X)		Chip_GPIO_WritePortBit(LPC_GPIO, QC_RST_PIN, (X))

#define QC_PLOT_NEW_SAMPLE	30
#endif /* __QC12864B_INC_QCHARDWARE_H_ */
