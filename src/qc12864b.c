/*!
 *  @file 	   qc12864b.c
 *  @brief     Firmware for QC12864B LCD model
 *  @details   Firmware functions for QC12864B, those include:
 *  		   Display initializacion.
 *  		   Clear display screen
 *  		   Input ASCII text
 *  		   Set pixels
 *  		   Draw an image
 *  @author    Federico Baigorria
 *  @version   0.2.0
 *  @date      03-24-2020
 *  @copyright GNU Public License.
 */

#include "qc12864b.h"
#include "qcBmp.h"

/** Private functions
 */

/**
@fn static void qcWrite(uint8_t qcMode, uint8_t qcData)
@detail Refer to Sitronix's ST7920 manual serial interfase communication chapter for details.

@param qcMode defines if a command (0xF8) or data (0xFA) is sent.
@param qcData data to be sent
@return void
*/
static void qcWrite(qcModes qcMode, uint8_t qcData){
	uint8_t qcBuff[3];

	///! Transmit enabled
	QC_ENABLE_SPI(QC_TRUE);
	///! SPI transmit buffer preparation
	qcBuff[0] = qcMode; 				///! Byte 1 = 11111 RW RS 0
	qcBuff[1] = qcData & 0xF0;			///! Byte 2 = D7 D6 D5 D4 0000
	qcBuff[2] = (qcData << 4) & 0xF0;	///! Byte 3 = D3 D2 D1 D0 0000
	///! Data transmit
	while(! QC_SEND_SPI(qcBuff, QC_DATA_LENGHT));
	///! Transmit disabled
	QC_ENABLE_SPI(QC_FALSE);
}

/**
@fn static void qcTextClear(void)
@detail Fill DDRAM with 0x20H and set DDRAM address counter (AC)
 	 	to 0x00. Only clears text from the display screen.
 	 	Needs to previously set the display mode to basic instructions.

@return void
*/
static void qcTextClear(void){
	///! Display clear
	qcWrite(QC_IRM, QC_CLEAR);

	#if QC_FRTOS
		///! OS delay
		vTaskDelay(QC_CLEAR_DLY/portTICK_RATE_MS);
	#else
		///! External delay. Blocking
		qcDelay = QC_FALSE;
		while(!qcDelay);
	#endif
}

/**
@fn static void qcSetPixel(uint8_t y_addr, uint8_t x_addr, uint16_t qcPixel)
@detail Sets a single or group of pixels in the display.

Refer to Sitronix's ST7920 manual graphic mode chapter for details.

Graphic mode explanation: The display is divided in two vertical parts. GDRAM vertical address
unit is bit and GDRAM horizontal address is byte. Each horizontal address contains in fact
two bytes. Horizontal addresses from 0 to 7 contains the "first vertical part" of the display
and addresses from 8 to 15 contains the left part.

Vertical cursor is fixed until a new DDRAM address is set

Horizontal cursor is set per byte and each time data is written to the display the horizontal
cursor is displaced 1 byte so two writes clears an entire horizontal address. Have in mind that
if horizontal cursor exceeds the two bytes writes it continues from the same horizontal address,
it does NOT increment the horizontal address index.

Note: Start base address is 0x80 (DDRAM Address)

@param x_addr column coordinate (horizontal address)
@param y_addr row coordinate (vertical address)
@param qcPixel pixel or group of pixels to be set in the horizontal address
@return void
*/
static void qcSetPixel(uint8_t y_addr, uint8_t x_addr, uint16_t qcPixel){
	///! Set instruction mode to extended
	qcWrite(QC_IRM, QC_SET_EXT);
	qcWrite(QC_IRM, QC_SET_EXT);
	///! Display graphic mode. Manufacturer advice to set this command twice
	qcWrite(QC_IRM, QC_GPH_ON);
	qcWrite(QC_IRM, QC_GPH_ON);
	///! Set X/Y coordinate. First address is Y then X (fixed by manufacturer)
	qcWrite(QC_IRM, ROW_BASE | y_addr);
	qcWrite(QC_IRM, ROW_BASE | x_addr);
	///! Horizontal address two bytes compound
	qcWrite(QC_DTM, (uint8_t) ((qcPixel >> 8) & 0x00FF));
	qcWrite(QC_DTM, (uint8_t) qcPixel & 0x00FF);
}

/**
@fn static void qcSetRow(uint8_t qcRow, uint16_t qcPixel)
@detail Sets or clears an entire 16 bits tall row. Four rows can be independently highlighted
		or cleared.

@param qcRow Row to be altered, values can be 1, 2, 3 or 4 (16 bits tall)
@param qcPixel Horizontal pixel value, all highlighted or cleared.
@return void
*/
static void qcSetRow(qcRows qcRow, uint16_t qcPixel){
	uint8_t y_addr, x_addr;
	uint8_t y_offs = 0;
	uint8_t x_offs = 0;

	///! Vertical address offset
	if(qcRow == qcRow4 || qcRow == qcRow2)
		y_offs = MAX_VER_ADDRESS/2;
	///! Horizontal address offset
	if(qcRow == qcRow3 || qcRow == qcRow4)
		x_offs = ROW_OFFSET;

	///! Row highlight (height 16 bits)
	for(y_addr = 0; y_addr < MAX_VER_ADDRESS/2; y_addr++){
		for(x_addr = 0; x_addr < MAX_HOR_ADDRESS / 2; x_addr++){
			qcSetPixel(ROW_BASE | y_addr | y_offs, \
						ROW_BASE | x_addr| x_offs, qcPixel);
		}
	}
}

#if QC_USE_SIGNAL
/**
@fn static void qcSetPlotAxis(void)
@detail Used by qcSignalPlot()
		Plots an X/Y axis 4 pixels from origin. This value is fixed (hardcoded)

@return void
*/
static void qcSetPlotAxis(void){
	qcXptr = 0x00;									///! Set X address pointer to origin
	qcDotidx = QC_X_DOT_OFFS;						///! Set Dot index to 3
	qcDots[qcDotidx - 1].qcPixel = QC_PLOT_X_ORIG;	///! Set previous dot to plot Y axis

	uint8_t x_addr, y_addr;
	///! Draw Y axis
	for(y_addr = 0; y_addr < MAX_VER_ADDRESS * 2; y_addr++){
		///! First display part or fold (Horizontal addresses 0 to 7)
		if(y_addr < MAX_VER_ADDRESS){
			qcSetPixel(ROW_BASE | y_addr, ROW_BASE, QC_PLOT_Y_ORIG);
		} else {
			qcSetPixel(ROW_BASE | (y_addr%32), ROW_BASE | ROW_OFFSET, QC_PLOT_Y_ORIG);
		}
	}
	///! Draw X axis
	for(x_addr = 0; x_addr < MAX_HOR_ADDRESS / 2; x_addr++){
		///! Second display part or fold (Horizontal addresses 8 to 15)
		qcSetPixel(ROW_BASE | 0x1B, ROW_BASE | ROW_OFFSET | x_addr, 0xFFFF);
	}
}

/**
@fn static void qcSortDots(void)
@detail Used by qcSignalPlot()
		Sort dots by Y addresses. A single dot is composed by to pixels. Since it can only
		be edited a group of 16 pixels at each write sequence in order to remember previous
		"dots" those must be accumulated. When a "new dot" has the same Y address then this dot
		will accumulate previous dots with the same Y address.

@return void
*/
static void qcSortDots(void){
	uint8_t i;
	///! Sort dots and accumulate previous values if Y address is repeated
	if(qcXptr > 0x00){ ///! For plotting dots after plotting the X origin address
		for(i = 0; i <= qcDotidx - 1; i++){
			if(qcDots[i].qcYaddr == qcDots[qcDotidx].qcYaddr){
				qcDots[qcDotidx].qcPixel |= qcDots[i].qcPixel;
			}
		}
	} else { 			///! After six pixels or three dots we start plotting Y values
		if(qcDotidx == QC_X_DOT_OFFS){ ///! Previous dot plots the Y axis part
			qcDots[qcDotidx].qcPixel |= qcDots[QC_X_DOT_OFFS - 1].qcPixel;
		}
		for(i = QC_X_DOT_OFFS; i <= qcDotidx - 1; i++){
			if(qcDots[i].qcYaddr == qcDots[qcDotidx].qcYaddr){
				qcDots[qcDotidx].qcPixel |= qcDots[i].qcPixel;
			}
		}
	}
}

/**
@fn static uint8_t qcRoundNewVal(void)
@detail Used by qcSignalPlot()
		Fetches a new Y value to be plotted, it's scaled into a possible Y dot
		value and rounded if needed.

@return void
*/
static uint8_t qcRoundNewVal(void){
	///! Linear scaling y = A*x + B
	float qcYval = (QC_PLOT_LINE_A * QC_PLOT_NEW_SAMPLE ) + QC_PLOT_LINE_B;
	uint8_t qcYvalr = (uint8_t) qcYval;

	///! Round up
	if((qcYval - qcYvalr) > 0.5){
		qcYvalr += 1;
	}
	///! Trimming
	if(qcYvalr > QC_PLOT_MAX_YDOT){
		return QC_PLOT_MAX_YDOT;			///! Trim by upper limit
	} else if(qcYvalr < QC_PLOT_MIN_YDOT){
		return QC_PLOT_MIN_YDOT;			///! Trim by lower limit
	}else {
		return qcYvalr;						///! No trim
	}
}

/**
@fn static void qcFormatNewDot(void)
@detail Used by qcSignalPlot()
		Receives a new Y dot to be plotted and it's loaded in the qcDots array

@return void
*/
static void qcFormatNewDot(void){
	///! Obtain scaled  Y address
	qcDots[qcDotidx].qcYaddr = qcRoundNewVal();
	///! X address offset to plot values above or below the middle point
	if(qcDots[qcDotidx].qcYaddr > MAX_VER_ADDRESS - 1){
		qcDots[qcDotidx].qcYaddr = (MAX_VER_ADDRESS - 1) - (qcDots[qcDotidx].qcYaddr % MAX_VER_ADDRESS);
		qcDots[qcDotidx].qcXoffs = 0x00;
	}else{
		qcDots[qcDotidx].qcYaddr = MAX_VER_ADDRESS - qcDots[qcDotidx].qcYaddr;
		qcDots[qcDotidx].qcXoffs = ROW_OFFSET;
	}
	///! Dot shifting
	qcDots[qcDotidx].qcPixel = QC_DOT_MASK >> (qcDotidx * 2);
}

/**
@fn static void qcPlotNewDot(void)
@detail Used by qcSignalPlot()
		Inserts a new dot. Can exist at least 8 possible dots with different Y addresses
		before changing the X address pointer qcXptr.
		qcXptr keeps track of the current X address position to print a new dot (8 maximum or
		16 pixels in total).

@return void
*/
static void qcPlotNewDot(void){
	///! Fetch a new Y value and format it as a dot
	qcFormatNewDot();
	qcSortDots();
	///! Plot dots
	if(qcXptr > 0x00){
		for(uint8_t i=0; i <= qcDotidx; i++){
			qcSetPixel(qcDots[i].qcYaddr, qcDots[i].qcXoffs | qcXptr, qcDots[i].qcPixel);
		}
	} else {
		for(uint8_t i=QC_X_DOT_OFFS; i <= qcDotidx; i++){
			qcSetPixel(qcDots[i].qcYaddr, qcDots[i].qcXoffs | qcXptr, qcDots[i].qcPixel);
		}
	}
	///! Increment X dot index qcDotidx
	qcDotidx++;
	///! Increment X address pointer qcXptr
	if(qcDotidx >= MAX_HOR_ADDRESS / 2){
		qcXptr++;
		qcDotidx = 0;
	}
}
#endif /* QC_USE_SIGNAL */

/** Public functions
 */

/**
@fn void qcInit(void)
@detail Inits display.

This function has the posibility to have a OS made delay or an external delay, the last one is
blocking. This function should not be interrupted and it must NOT be interrupted if there's
another SPI device in the same bus.
@return void
*/
void qcInit(void){
	///! LCD reset cycle. Active delay
	QC_PINRESET(QC_FALSE);
	for(uint16_t i=0; i<60000; i++);
	QC_PINRESET(QC_TRUE);
	for(uint16_t i=0; i<60000; i++);

	///! Function set. Manufacturer advice to set this command twice
	qcWrite(QC_IRM, QC_SET_BASIC);
	qcWrite(QC_IRM, QC_SET_BASIC);
	///! Display ON
	qcWrite(QC_IRM, QC_DISP_ON);
	///! Display clear text
	qcTextClear();

	///! Entry mode
	qcWrite(QC_IRM, QC_ENTRY);
	///! Display control
	qcWrite(QC_IRM, QC_DISP_CTRL);
	///! Positioning
	qcWrite(QC_IRM, QC_SET_DDRAM);
}

/**
@fn void void qcSetText(char *txt, uint8_t qcRow)
@detail Write ASCII characters at rows starting positions.

@param *txt pointer to ascii string.
@param row start position of said row
@return void
*/
void qcSetText(char *txt, uint8_t qcRow){
	///! Display text mode
	qcWrite(QC_IRM, QC_SET_BASIC);
	qcWrite(QC_IRM, QC_SET_BASIC);
	///! Set DDRAM Address as row position
	qcWrite(QC_IRM, qcRow);

	while(*txt){
		qcWrite(QC_DTM, (uint8_t)(*txt));
		txt++;
	}
}

/**
@fn void qcClear(void)
@detail Clear display.

This function clears vertically each horizontal address until all horizontal addresses are
cleared.
@return void
*/
void qcClear(void){
	///! Display clear text (for text mode)
	qcWrite(QC_IRM, QC_SET_BASIC);
	qcWrite(QC_IRM, QC_SET_BASIC);
	qcTextClear();

	///! Display clear scan (for graphic mode)
	for(uint8_t x = 0;x < MAX_HOR_ADDRESS; x++){
		for(uint8_t y = 0; y < MAX_VER_ADDRESS; y++){
			qcSetPixel(ROW_BASE | y, ROW_BASE | x, 0x0000);
		}
	}
}

#if QC_USE_BMP
/**
@fn void qcSetImage(void)
@detail Prints a static bmp image in the display.

@return void
*/
void qcSetImage(void){
	uint8_t x, y;			///! X,Y coordinates
	uint16_t qcPixel = 0;	///! Pixel
	uint16_t bpix = 0; 		///! number of pixels in bytes (1024)

	///! Horizontally set bit map
	for(y=0; y < MAX_VER_ADDRESS * 2; y++){
		for(x=0; x < MAX_HOR_ADDRESS / 2; x++){
			///! Conform 16 bits pixel
			qcPixel = (qcBmpArray[2*bpix] << 8) | qcBmpArray[2*bpix+1];

			if(y < MAX_VER_ADDRESS){
			///! First display part or fold (Horizontal addresses 0 to 7)
				qcSetPixel(ROW_BASE | y, ROW_BASE | x, qcPixel);
			} else {
			///! Second display part or fold (Horizontal addresses 8 to 15)
				qcSetPixel(ROW_BASE | (y%32), ROW_BASE | ROW_OFFSET | x, qcPixel);
			}

			bpix++;
		}
	}
}
#endif

/**
@fn void qcSetHighlight(uint8_t qcRow)
@detail Highlights an entire row. Four rows can be highlighted.

@return void
*/
void qcSetHighlight(qcRows qcRow){
	qcSetRow(qcRow, 0xFFFF);
}

/**
@fn void qcClearHighlight(uint8_t qcRow)
@detail Clears an entire row. Four rows can be cleared.

@return void
*/
void qcClearHighlight(qcRows qcRow){
	qcSetRow(qcRow, 0x0000);
}

#if QC_MEM_DYN
/**
@fn void qcSetDynamicImage(qcLCDArray *qcArray, uint8_t qcClearFirst)
@detail Dynamically set pixels in the display from a dynamic qcArray. If its needed the
display can be cleared before printing.

@todo For future development, set pixel by coordinate maybe with stack usage.
	  DO NOT USE.
@return void
*/
void qcSetDynamicImage(qcLCDArray qcArray, uint8_t qcClearFirst){
	if(qcClearFirst)
		qcClear();

	//qcSetPixel(qcArray.qcYaddr, qcArray.qcXaddr, qcArray.qcPixel);
}
#endif

/**
@fn void qcSignalPlot(void)
@detail Starts a new signal plot. Each new Y value is printed as a two pixel dot, this plot
		is refreshed each QC_REFRESH_GPH ms. When the qcXptr pointer has reached the maximum
		X address vector the plot is restarted.

@return void
*/
void qcSignalPlot(void){

	qcSetPlotAxis();
	while(1)
	{
		///! Configures a new dot to be plotted
		qcPlotNewDot();
		///! Graphics a new pixel group every QC_REFRESH_GPH ms
		#if QC_FRTOS
			vTaskDelay(QC_REFRESH_GPH / portTICK_RATE_MS);
		#else
			///! TODO
		#endif

		///! Init a new plot cycle
		if(qcXptr >= MAX_HOR_ADDRESS / 2){
			qcClear();	///! cambiar a un qcClear sin tocar los ejes
			qcSetPlotAxis();
		}
	}
}


/**
@fn void qcTest(void)
@detail Starts a full LCD test, verifies full text and graphic functions. Since this function
		may init() the LCD it's should be used once for special cases and not for a normal display
		usage.

@todo Modify this function so it can be used without an SO

@return void
*/

#if QC_USE_TEST

void qcTest(void){
	///! Init QC12864B
	qcInit();

	///! 1st test. Graphical mode
	qcSetText((char*)"   Starting test", qcRowBase_1);
	qcSetText((char*)"      graph mode", qcRowBase_2);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcClear();

	qcSetHighlight(qcRow1);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetHighlight(qcRow2);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetHighlight(qcRow3);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetHighlight(qcRow4);
	vTaskDelay(1000 / portTICK_RATE_MS);

	qcClearHighlight(qcRow4);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcClearHighlight(qcRow3);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcClearHighlight(qcRow2);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcClearHighlight(qcRow1);
	vTaskDelay(1000 / portTICK_RATE_MS);

	///! 2nd test Text mode
	qcSetText((char*)"abcdefghijklmnop", qcRowBase_1);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetText((char*)"qrstuvwxyz", qcRowBase_2);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetText((char*)"ABCDEFGHIJKLMNOP", qcRowBase_3);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetText((char*)"QRSTUVWXYZ", qcRowBase_4);
	vTaskDelay(1000 / portTICK_RATE_MS);

	qcClear();

	qcSetText((char*)"1234567890", qcRowBase_1);
	vTaskDelay(1000 / portTICK_RATE_MS);
	qcSetText((char*)"-+[]/*", qcRowBase_2);
	vTaskDelay(1000 / portTICK_RATE_MS);

	qcClear();
	qcSetText((char*)"Test finished", qcRowBase_1);
	vTaskDelay(1000 / portTICK_RATE_MS);

	qcClear();
}

#endif










